import os
import yaml
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from utils.db import db
from app.api import bp as api


def create_app(config_name, config_path=None):
    # 创建flask项目
    app = Flask(__name__)

    # 支持跨域
    CORS(app)

    # 注册蓝图
    app.register_blueprint(api, url_prefix='/api')

    # 读取配置文件
    if not config_path:
        pwd = os.getcwd()
        config_path = os.path.join(pwd, "config/config.yaml")
    if not config_name:
        config_name = "PRODUCTION"
    conf = read_yaml(config_name, config_path)
    app.config.update(conf)

    # 连接数据库
    # db = SQLAlchemy()
    db.app = app
    db.init_app(app)

    return app


def read_yaml(config_name, config_path):
    """
    config_name:需要读取的配置内容
    config_path:配置文件路径
    """
    if config_name and config_path:
        with open(config_path, "r", encoding="utf-8") as f:
            conf = yaml.safe_load(f.read())
        if config_name in conf.keys():
            return conf[config_name.upper()]
        else:
            raise KeyError("未找到对应的配置信息")
    else:
        raise ValueError("请输入正确的配置名称或配置文件路径")
