import os
import pandas
from flask import Blueprint, request, g, jsonify
from werkzeug.utils import secure_filename

from utils.db import db
from models.model import Users, Accounts, Budgets, Records
import utils.modify as modify
from utils.jsonWebToken import login_required, create_token

bp = Blueprint("api", __name__)


# 测试数据库连接
@bp.route("/dbtest", methods=["GET"])
def test():
    users = Users.query.filter(Users.email == '1@1.com').first()
    print(type(users))
    return "OK"


@bp.route("/test", methods=["GET"])
def dbtest():
    return "test OK"


@bp.route("/login_register", methods=["POST"])
def login():
    data = request.get_json()
    q = Users.query.filter(Users.email == data['email']).first()
    # 若用户不存在
    if not q:
        # 新建用户
        modify.create_user(data['email'], data['password'])

        user = db.session.query(Users).filter(Users.email == data['email']).first()

        new_token = create_token(user.id)
        print(new_token)
        return {
            "msg": "user added",
            "token": new_token,
            "id": user.id,
            "username": user.username,
            "email": user.email,
        }
    # 若用户已存在
    else:
        user = db.session.query(Users).filter(Users.email == data['email']).first()
        new_token = create_token(user.id)
        print(new_token)
        return {
            "msg": "user exists",
            "token": new_token,
            "id": user.id,
            "username": user.username,
            "email": user.email,
        }


@bp.route('/get_info', methods=['GET'])
@login_required
def get_info():
    return modify.get_info()


# @bp.route('/account', methods=['GET'])
# @login_required
# def account_get():
#     data = request.get_json()
#     modify.create_account(data['userId'], data['type'], data['accountName'])
#     return "ok"


@bp.route('/ledger', methods=['POST'])
@login_required
def ledger_post():
    data = request.get_json()
    modify.create_ledger(g.userId, data['name'])
    return "ok"


@bp.route('/account', methods=['POST'])
@login_required
def account_post():
    data = request.get_json()
    print(data)
    modify.create_account(data['ledgerId'], data['type'], data['accountName'])
    return "ok"


#
# @bp.route('/account', methods=['PUT'])
# @login_required
# def account_put():
#     data = request.get_json()
#     modify.create_account(data['userId'], data['type'], data['accountName'])
#     return "ok"
#
#
# @bp.route('/account', methods=['DELETE'])
# @login_required
# def account_delete():
#     data = request.get_json()
#     modify.create_account(data['userId'], data['type'], data['accountName'])
#     return "ok"
#
#


# @bp.route('/budget', methods=['GET'])
# @login_required
# def create_budget():
#     data = request.get_json()
#     modify.create_budget(g.userId, data['type'], data['name'])
#     return "ok"
@bp.route('/budget', methods=['POST'])
@login_required
def budget_post():
    data = request.get_json()
    print(data)
    modify.create_budget(data['id'], data['type'], data['name'], data['demand'])
    return "ok"


# @bp.route('/budget', methods=['PUT'])
# @login_required
# def create_budget():
#     data = request.get_json()
#     modify.create_budget(g.userId, data['type'], data['name'])
#     return "ok"
#
#
# @bp.route('/budget', methods=['DELETE'])
# @login_required
# def create_budget():
#     data = request.get_json()
#     modify.create_budget(g.userId, data['type'], data['name'])
#     return "ok"


# @bp.route('/record', methods=['GET'])
# @login_required
# def add_record():
#     data = request.get_json()
#     modify.add_record(data['accountId'], data['isIncome'], data['amount'], data['usage'], data['desc'])
#     return "ok"


@bp.route('/record', methods=['POST'])
@login_required
def record_post():
    data = request.get_json()
    print(data)
    modify.create_record(g.userId, data['accountId'], data['budgetId'], data['isIncome'], data['amount'], data['desc'],
                         data['payee'])
    return "ok"


# @bp.route('/record', methods=['PUT'])
# @login_required
# def add_record():
#     data = request.get_json()
#     modify.add_record(data['accountId'], data['isIncome'], data['amount'], data['usage'], data['desc'])
#     return "ok"


# @bp.route('/record', methods=['DELETE'])
# @login_required
# def add_record():
#     data = request.get_json()
#     modify.add_record(data['accountId'], data['isIncome'], data['amount'], data['usage'], data['desc'])
#     return "ok"


@bp.route('/budget', methods=['PUT'])
def budget_put():
    data = request.get_json()
    print(data)
    if data['msg'] == 'reset':
        modify.reset_budget(data['id'])
        return "ok"
    else:
        modify.assign(data['from'],data['to'], data['amount'])
        return "no"


@bp.route('/ledger/<ledger_id>', methods=['DELETE'])
def ledger_delete(ledger_id):
    modify.delete_ledger(ledger_id)
    return "ok"


@bp.route('/account/<account_id>', methods=['DELETE'])
def account_delete(account_id):
    modify.delete_account(account_id)
    return "ok"


@bp.route('/budget/<budget_id>', methods=['DELETE'])
def budget_delete(budget_id):
    modify.delete_budget(budget_id)
    return "ok"


@bp.route('/upload_csv', methods=["POST"])
@login_required
def upload_csv():
    """
    测试登陆保护下获取数据
    :return:
    """
    file = request.files['file']
    ledger_id = request.form['ledgerId']
    assign = Budgets.query.filter_by(ledgerId=ledger_id, type=0).first()
    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400
    if file:
        user_id = g.userId
        account = Accounts.query.filter_by(ledgerId=ledger_id, name='支付宝').first()
        file.save(os.path.join('.\\', 'ali.csv'))
        file_path = '.\\ali.csv'
        df = pandas.read_csv(file_path, on_bad_lines='skip', encoding='gbk', header=22)

        # 查看前几行数据确保正确读取
        for index, row in df.iterrows():
            if row['收/支'] == '收入':
                modify.create_record(user_id, account.id, assign.id, True, row['金额'],
                                     row['商品说明'], row['交易对方'], row['交易时间'])
            if row['收/支'] == '支出;/':
                modify.create_record(user_id, account.id, assign.id, False, row['金额'],
                                     row['商品说明'], row['交易对方'], row['交易时间'])
        return jsonify({"success": "File uploaded successfully"}), 200
    return "你好！！"
