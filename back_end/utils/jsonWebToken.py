from functools import wraps

from flask import Flask, request, jsonify,g
import jwt
from jwt import exceptions

import datetime

key = "zkpfw*%$qjrfono@sdko34@%"

# 构造header
headers = {
    'typ': 'jwt',
    'alg': 'HS256'
}


def create_token(id):
    # 构造payload
    payload = {
        'id': id,
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7)  # 超时时间
    }
    result = jwt.encode(payload=payload, key=key, algorithm="HS256", headers=headers)
    return result


def identify(auth_header: str):
    """
    用户鉴权
    :return:
    """
    if auth_header:
        try:
            payload = jwt.decode(auth_header, key=key, algorithms=['HS256'])
        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, jwt.InvalidSignatureError):
            return False
        if not payload:
            return False
        if "id" in payload and "exp" in payload:
            g.userId = payload["id"]
            return payload["id"]
        else:
            return False


def login_required(f):
    """
    登陆保护，验证用户是否登陆
    :param f:
    :return:
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.headers.get("Authorization", default=None)
        if not token:
            return "请登陆"
        user_id = identify(token)
        if not user_id:
            return "请登陆"
        return f(*args, **kwargs)

    return wrapper
