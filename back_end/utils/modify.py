from flask import jsonify, g
from models.model import Accounts, Users, Records, Budgets, Ledgers
from utils.db import db


def get_info():
    result = {}

    # 向 result 中添加 Ledger 数据
    ledger_list = []
    ledgers = Ledgers.query.filter(Ledgers.userId == g.userId).all()
    for ledger in ledgers:
        ledger_list.append(ledger.to_json())
    result.update({"ledger": ledger_list})

    # 向 result 中添加 Account 数据
    account_list = []
    for ledger in ledgers:
        accounts = Accounts.query.filter(Accounts.ledgerId == ledger.id).all()
        for account in accounts:
            account_list.append(account.to_json())
    result.update({"account": account_list})

    #  向 result 中添加 Record 数据，按照日期降序排列
    record_list = []
    records = Records.query.filter(Records.userId == g.userId).order_by(Records.time.asc()).all()
    for record in records:
        record_list.append(record.to_json())
    result.update({"record": record_list})

    # 向 result 中添加 Budget 数据
    budget_list = []
    for ledger in ledgers:
        budgets = Budgets.query.filter(Budgets.ledgerId == ledger.id).all()
        for budget in budgets:
            budget_list.append(budget.to_json())
    result.update({"budget": budget_list})

    # 向 result 中添加 User 数据
    user = db.session.query(Users).filter(Users.id == g.userId).first()
    result.update({"user": user.to_json()})

    print(result)

    return jsonify(result)


def create_user(user_email, user_password):
    # 创建用户
    new_user = Users()
    new_user.email = user_email
    new_user.password = user_password
    db.session.add(new_user)
    db.session.commit()
    create_ledger(new_user.id)


def create_ledger(user_id, name='Default'):
    # 创建默认账本
    new_ledger = Ledgers()
    new_ledger.userId = user_id
    new_ledger.name = name
    db.session.add(new_ledger)
    db.session.commit()

    # 创建默认账户
    account_list = [
        {
            'name': "微信",
            'type': 0,
        },
        {
            'name': "支付宝",
            'type': 0,
        },
        {
            'name': "现金",
            'type': 1,
        },
        {
            'name': "银行卡",
            'type': 2,
        },
        {
            'name': "花呗",
            'type': 3,
        },
        {
            'name': "白条",
            'type': 3,
        }
    ]

    # 创建预算
    budget_list = [
        {
            'name': "待分配资金",
            'type': 0,
        },
        {
            'name': "房租",
            'type': 1,
        },
        {
            'name': "水电",
            'type': 1,
        },
        {
            'name': "话费",
            'type': 1,
        },
        {
            'name': "食物",
            'type': 2,
        },
        {
            'name': "汽油",
            'type': 2,
        },
        {
            'name': "娱乐",
            'type': 3,
        },
        {
            'name': "新车",
            'type': 3,
        },
        {
            'name': "存款",
            'type': 4,
        },
    ]

    for item in account_list:
        new_account = Accounts()
        new_account.ledgerId = new_ledger.id
        new_account.name = item.get('name')
        new_account.type = item.get('type')
        db.session.add(new_account)
        db.session.commit()

    for item in budget_list:
        new_budget = Budgets()
        new_budget.ledgerId = new_ledger.id
        new_budget.name = item.get('name')
        new_budget.type = item.get('type')
        db.session.add(new_budget)
        db.session.commit()


def create_account(ledger_id, account_type, account_name):
    new_account = Accounts()
    new_account.ledgerId = ledger_id
    new_account.type = account_type
    new_account.name = account_name
    db.session.add(new_account)
    db.session.commit()


def create_budget(ledger_id, budget_type, budget_name, budget_demand):
    new_budget = Budgets()
    new_budget.ledgerId = ledger_id
    new_budget.type = budget_type
    new_budget.name = budget_name
    new_budget.demand = budget_demand
    db.session.add(new_budget)
    db.session.commit()


def create_record(user_id, account_id, budget_id, is_income, amount, desc, payee, time=None):
    # 添加记录
    account = Accounts.query.filter_by(id=account_id).first()
    budget = Budgets.query.filter_by(id=budget_id).first()
    ledger = Ledgers.query.filter_by(id=budget.ledgerId).first()
    if account and (budget or budget_id == 0):
        new_record = Records()
        new_record.userId = user_id
        new_record.ledgerId = ledger.id
        new_record.accountId = account_id
        new_record.isIncome = is_income
        new_record.amount = amount
        new_record.payee = payee
        new_record.desc = desc
        if time:
            new_record.time = time
        db.session.add(new_record)
        db.session.commit()

        # 更新余额
        if is_income:
            account.balance += new_record.amount
        else:
            account.balance -= new_record.amount
        db.session.commit()

        # 更新预算
        budget = Budgets.query.filter_by(id=budget_id).first()
        if budget.type != 0:
            if is_income:
                budget.assigned += new_record.amount
            else:
                budget.outflow += new_record.amount
        else:
            if is_income:
                budget.assigned += new_record.amount
            else:
                budget.assigned -= new_record.amount
        db.session.commit()

        # 更新 stats
        userid = g.userId
        user = Users.query.filter_by(id=userid).first()
        user.numRecords += 1
        db.session.commit()
        return True
    else:
        return False


def reset_budget(budget_id):
    budget = Budgets.query.filter_by(id=budget_id).first()
    assign = Budgets.query.filter_by(ledgerId=budget.ledgerId, type=0).first()
    assign.assigned += budget.assigned - budget.outflow
    budget.assigned = 0
    budget.outflow = 0
    db.session.commit()
    return True


def assign(from_id, to_id, amount):
    ledger = Ledgers.query.filter_by(id=from_id).first()
    from_budget = Budgets.query.filter_by(ledgerId=ledger.id, type=0).first()
    to_budget = Budgets.query.filter_by(id=to_id).first()
    from_budget.assigned -= int(amount)
    to_budget.assigned += int(amount)
    db.session.commit()
    return True


def delete_ledger(ledger_id):
    ledger = Ledgers.query.filter_by(id=ledger_id).first()
    budgets = Budgets.query.filter_by(ledgerId=ledger_id).all()
    accounts = Accounts.query.filter_by(ledgerId=ledger_id).all()
    for account in accounts:
        records = Records.query.filter_by(accountId=account.id).all()
        for record in records:
            db.session.delete(record)
        db.session.delete(account)
    for budget in budgets:
        db.session.delete(budget)
    db.session.delete(ledger)
    db.session.commit()
    return True


def delete_account(account_id):
    account = Accounts.query.filter_by(id=account_id).first()
    records = Records.query.filter_by(accountId=account_id).all()
    for record in records:
        db.session.delete(record)
    db.session.delete(account)
    db.session.commit()
    return True

def delete_budget(budget_id):
    budget = Budgets.query.filter_by(id=budget_id).first()
    assign = Budgets.query.filter_by(ledgerId=budget.ledgerId, type=0).first()
    assign.assigned += budget.assigned - budget.outflow
    db.session.delete(budget)
    db.session.commit()
    return True
