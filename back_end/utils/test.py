import pandas as pd

# 假设 CSV 文件名为 'alipay_record_202009.csv'，使用适当的编码
file_path = '../ali.csv'
df = pd.read_csv(file_path, on_bad_lines='skip', encoding='gbk', header=22)  # 或者尝试 encoding='utf-8' 如果 'gbk' 不工作

# 查看前几行数据确保正确读取
for index, row in df.iterrows():
    print(row['金额'])