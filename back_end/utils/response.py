class ResponseCode:
    SUCCESS = 0
    FAILURE = -1

class ResponseMessage:
    SUCCESS = "成功"
    FAILURE = "失败"

class Response:
    def __init__(self):
        self.code = None
        self.message = None
        self.data = None
    
    def update(self,code, message, data):
        self.code = code
        self.message = message
        self.data = data

    def data(self):
        return {"code": self.code, "message": self.message, "data": self.data}




