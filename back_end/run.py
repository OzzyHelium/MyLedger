from app.factory import create_app

app=create_app(config_name='DEVELOPMENT')

if __name__ == '__main__':
    print('http://127.0.0.1:5000/')
    app.run(host='0.0.0.0')
