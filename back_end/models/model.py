import datetime

from utils.db import db


class Users(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    username = db.Column(db.String(50), nullable=True)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    register = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)
    numRecords = db.Column(db.Integer, default=0, nullable=False)

    # 转换为json
    def to_json(self):
        json = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'password': self.password,
            'register': self.register,
            'numRecords': self.numRecords
        }
        return json


class Ledgers(db.Model):
    __tablename__ = 'ledgers'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    userId = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(50), nullable=True)

    def to_json(self):
        json = {
            'id': self.id,
            'userId': self.userId,
            'name': self.name
        }
        return json


class Accounts(db.Model):
    __tablename__ = 'accounts'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    ledgerId = db.Column(db.Integer, nullable=True)
    name = db.Column(db.String(50), nullable=True)
    balance = db.Column(db.Float, default=0, nullable=False)
    type = db.Column(db.String(50), default=0, nullable=False)

    def to_json(self):
        json = {
            'id': self.id,
            'ledgerId': self.ledgerId,
            'name': self.name,
            'balance': self.balance,
            'type': self.type
        }
        return json


class Budgets(db.Model):
    __tablename__ = 'budgets'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    ledgerId = db.Column(db.Integer, nullable=True)
    name = db.Column(db.String(50), nullable=True)
    demand = db.Column(db.Float, default=0, nullable=False)
    assigned = db.Column(db.Float, default=0, nullable=False)
    outflow = db.Column(db.Float, default=0, nullable=False)
    type = db.Column(db.Integer, default=0, nullable=False)

    def to_json(self):
        json = {
            'id': self.id,
            'ledgerId': self.ledgerId,
            'name': self.name,
            'demand': self.demand,
            'assigned': self.assigned,
            'outflow': self.outflow,
            'type': self.type
        }
        return json


class Records(db.Model):
    __tablename__ = 'records'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    accountId = db.Column(db.Integer, nullable=False)
    userId = db.Column(db.Integer, nullable=True)
    ledgerId = db.Column(db.Integer, nullable=True)
    isIncome = db.Column(db.Boolean, default=0, nullable=False)
    amount = db.Column(db.Float, default=0, nullable=False)
    payee = db.Column(db.String(50), nullable=True)
    desc = db.Column(db.String(100), nullable=True)
    time = db.Column(db.DateTime, default=datetime.datetime.now, nullable=False)

    def to_json(self):
        json = {
            'id': self.id,
            'userId': self.userId,
            'ledgerId': self.ledgerId,
            'accountId': self.accountId,
            'isIncome': self.isIncome,
            'amount': self.amount,
            'payee': self.payee,
            'desc': self.desc,
            'time': self.time
        }

        return json
