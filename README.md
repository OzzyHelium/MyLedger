# MyLedger

一个基于flask和Vue.js的记账应用。

## 安装

```cmd
cd backend
pip install flask, flask_cors, 
```

## 技术栈

- Python：后端使用Python的Flask框架。
- Vue.js：前端使用Vue.js框架。
- Daisy UI：前端UI库使用Daisy UI。
- Axios：用于发起HTTP请求。
- Pinia：用于状态管理。
- Vue Router：用于路由管理。
- Flask-CORS：用于解决跨域问题。
- SQLAlchemy：用于数据库操作。

## 功能

- 记账：用户可以添加收入和支出记录。
- 分类管理：用户可以创建和管理不同的分类，如餐饮、购物、旅行等。
- 统计分析：提供不同时间范围的统计数据，如月度、季度、年度等。
- 数据导出：用户可以导出数据到Excel文件。
