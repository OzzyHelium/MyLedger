import axios from 'axios';
import router from '../router';
import { url_prefix } from '../config/index'
import { useUserStore } from '@/stores/user'
import { useInfoStore } from '@/stores/info'

const userStore = useUserStore()
const infoStore = useInfoStore()

const useAxios = async (url, data={}, method = 'GET') => {
    const token = userStore.token
    try {
        const a = url_prefix+'/api' + url
        // GET
        if (method === 'GET') {
            await axios.get(a, {
                headers: { Authorization: token }
            }).then(function (response) {
                // 将信息写入store
                console.log(response.data)
                userStore.setUser(response.data.user)
                infoStore.setAccount(response.data.account)
                infoStore.setBudget(response.data.budget)
            })
        }

        // POST
        else if (method === 'POST') {
            await axios.post(a, data, {
                headers: { Authorization: token }
            }).then(function (response) {
                console.log(response.data)
                // router.push('/loading')
            })
        }

        // PUT
        else if (method === 'PUT') {
            await axios.put(a, data, {
                headers: { Authorization: token }
            }).then(function (response) {

            })
        }

        // DELETE
        else if (method === 'DELETE') {
            await axios.delete(a)
            .then(response => {
              console.log('Deleted:', response.data);
            })
            .catch(error => {
              console.error('Error:', error);
            });
        }

    } catch (error) {
        console.error(error)
        router.push('/error')
    }
}



export { useAxios }