import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useInfoStore = defineStore('info', () => {

    // record
    const recordList = []

    function setRecord(data) {
        recordList.length = 0
        for (let i = 0; i < data.length; i++) {
            recordList.push(data[i])
        }
    }

    // ledger
    const currentLedger = ref(0)
    const ledgerList = []
    function setLedger(data) {
        ledgerList.length = 0
        for (let i = 0; i < data.length; i++) {
            ledgerList.push(data[i])
        }
        console.log(ledgerList)
    }

    // account
    const currentAccount = ref(0)
    const accountList = []

    function setCurrentAccount(id) {
        currentAccount.value = id
    }

    function setAccount(data) {
        accountList.length = 0
        for (let i = 0; i < data.length; i++) {
            accountList.push(data[i])
        }
    }

    // budgets
    const budgetList = []
    function setBudget(data) {
        budgetList.length = 0
        for (let i = 0; i < data.length; i++) {
            budgetList.push(data[i])
        }
    }


    // 退出登录
    function logOut() {
        recordList.length = 0
        ledgerList.length = 0
        accountList.length = 0
    }

    return {
        recordList,
        setRecord,
        ledgerList,
        setLedger,
        currentLedger,
        currentAccount,
        setCurrentAccount,
        accountList,
        setAccount,
        budgetList,
        setBudget,
        logOut
    }
})
