import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
    const id = ref(0)
    const username = ref('')
    const email = ref('')
    const token = ref('')
    const assign = ref(0)
    const register = ref(0)
    const numRecords = ref(0)


    function setUser(data) {
        if (data.token){
            token.value = data.token
            sessionStorage.setItem('token', data.token);
        }
        sessionStorage.setItem('username', data.username);
        sessionStorage.setItem('id', data.id);
        sessionStorage.setItem('email', data.email);
        sessionStorage.setItem('register', data.register);
        sessionStorage.setItem('numRecords', data.numRecords);

        id.value = data.id
        username.value = data.username
        email.value = data.email
        register.value = data.register
        numRecords.value = data.numRecords
    }

    function logOut() {
        sessionStorage.setItem('token', 0);
        sessionStorage.setItem('username', 0);
        sessionStorage.setItem('assign', 0);
        sessionStorage.setItem('id', 0);
        sessionStorage.setItem('email', 0);
        sessionStorage.setItem('register', 0);
        sessionStorage.setItem('numRecords', 0);

        id.value = 0
        username.value = ''
        email.value = ''
        token.value = ''
        assign.value = 0
        register.value = 0
        numRecords.value = 0
    }

    return { id, username,assign, email, token, register, numRecords, setUser, logOut }
})
